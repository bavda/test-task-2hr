<?php

namespace App\Services;

use App\Enums\GameResultsEnum;
use App\Enums\RockPaperScissorsMoveEnum;
use App\Services\GameRules\Interfaces\IGameRule;

class GameService
{
    const GAME_ROUNDS = 100;

    /**
     * @var array<IGameRule>
     */
    private array $rules = [];

    function __construct(
        private readonly PlayerService $player1,
        private readonly PlayerService $player2
    ) {}

    public function loadRule(IGameRule $gameRule): void
    {
        $this->rules[] = $gameRule;
    }

    function playRound(): void
    {
        $allMoves = RockPaperScissorsMoveEnum::cases();
        $moveFirst = $this->player1->chooseMove();
        $moveSecond = $allMoves[array_rand($allMoves)];

        foreach ($this->rules as $rule) {
            $gameResult = $rule->play($moveFirst, $moveSecond);
            if ($gameResult !== null) {
                switch ($gameResult) {
                    case GameResultsEnum::FIRST_WIN:
                        $this->player1->incrementWins();
                        $this->player2->incrementLosses();
                        break;
                    case GameResultsEnum::SECOND_WIN:
                        $this->player1->incrementLosses();
                        $this->player2->incrementWins();
                        break;
                    case GameResultsEnum::DRAW:
                        $this->player1->incrementDraws();
                        $this->player2->incrementDraws();
                        break;
                }
                break;
            }
        }
    }


    function playGame(): void
    {
        for ($i = 0; $i < self::GAME_ROUNDS; $i++) {
            $this->playRound();
        }
    }

    public function getStatistics(): array
    {
        return [
            'Player 1' => $this->player1->getStatistics(),
            'Player 2' => $this->player2->getStatistics(),
        ];
    }
}
