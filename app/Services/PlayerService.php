<?php

namespace App\Services;

use App\Enums\RockPaperScissorsMoveEnum;

class PlayerService {
    private int $wins = 0;
    private int $losses = 0;
    private int $draws = 0;


    // TODO move to separated class
    function chooseMove(): RockPaperScissorsMoveEnum
    {
        return RockPaperScissorsMoveEnum::ROCK;
    }

    function incrementWins(): void
    {
        $this->wins++;
    }

    function incrementLosses(): void
    {
        $this->losses++;
    }

    function incrementDraws(): void
    {
        $this->draws++;
    }

    // TODO use DTO
    function getStatistics(): array
    {
        return [
            'wins' => $this->wins,
            'losses' => $this->losses,
            'draws' => $this->draws,
        ];
    }
}
