<?php

namespace App\Services\GameRules\Interfaces;

use App\Enums\GameResultsEnum;
use App\Enums\RockPaperScissorsMoveEnum;

interface IGameRule
{
    public function play(RockPaperScissorsMoveEnum $moveFirst, RockPaperScissorsMoveEnum $moveSecond): ?GameResultsEnum;
}
