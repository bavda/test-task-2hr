<?php

namespace App\Services\GameRules;

use App\Enums\GameResultsEnum;
use App\Enums\RockPaperScissorsMoveEnum;
use App\Services\GameRules\Interfaces\IGameRule;

class DrawRule implements IGameRule
{
    public function play(RockPaperScissorsMoveEnum $moveFirst, RockPaperScissorsMoveEnum $moveSecond): ?GameResultsEnum
    {
        return $moveFirst === $moveSecond ? GameResultsEnum::DRAW : null;
    }
}
