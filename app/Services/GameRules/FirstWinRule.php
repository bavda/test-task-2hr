<?php

namespace App\Services\GameRules;

use App\Enums\GameResultsEnum;
use App\Enums\RockPaperScissorsMoveEnum;
use App\Services\GameRules\Interfaces\IGameRule;

class FirstWinRule implements IGameRule
{

    public function play(RockPaperScissorsMoveEnum $moveFirst, RockPaperScissorsMoveEnum $moveSecond): ?GameResultsEnum
    {
        if (
            ($moveFirst === RockPaperScissorsMoveEnum::ROCK && $moveSecond === RockPaperScissorsMoveEnum::SCISSORS) ||
            ($moveFirst === RockPaperScissorsMoveEnum::PAPER && $moveSecond === RockPaperScissorsMoveEnum::ROCK) ||
            ($moveFirst === RockPaperScissorsMoveEnum::SCISSORS && $moveSecond === RockPaperScissorsMoveEnum::PAPER)
        ) {
            return GameResultsEnum::FIRST_WIN;
        } else {
            return null;
        }
    }
}
