<?php

namespace App\Services\GameRules;

use App\Enums\GameResultsEnum;
use App\Enums\RockPaperScissorsMoveEnum;
use App\Services\GameRules\Interfaces\IGameRule;

class SecondWinRule implements IGameRule
{
    public function play(RockPaperScissorsMoveEnum $moveFirst, RockPaperScissorsMoveEnum $moveSecond): ?GameResultsEnum
    {
        if (
            ($moveFirst === RockPaperScissorsMoveEnum::ROCK && $moveSecond === RockPaperScissorsMoveEnum::PAPER) ||
            ($moveFirst === RockPaperScissorsMoveEnum::PAPER && $moveSecond === RockPaperScissorsMoveEnum::SCISSORS) ||
            ($moveFirst === RockPaperScissorsMoveEnum::SCISSORS && $moveSecond === RockPaperScissorsMoveEnum::ROCK)
        ) {
            return GameResultsEnum::SECOND_WIN;
        } else {
            return null;
        }
    }
}
