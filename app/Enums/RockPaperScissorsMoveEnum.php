<?php

namespace App\Enums;

enum RockPaperScissorsMoveEnum: string
{
    case ROCK = "ROCK";
    case PAPER = "PAPER";
    case SCISSORS = "SCISSORS";
}
