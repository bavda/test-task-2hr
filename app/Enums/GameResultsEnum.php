<?php

namespace App\Enums;

enum GameResultsEnum: string
{
    case FIRST_WIN = "FIRST_WIN";
    case SECOND_WIN = "SECOND_WIN";
    case DRAW = "DRAW";
}
