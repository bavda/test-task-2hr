<?php

namespace App\Console\Commands;

use App\Services\GameRules\DrawRule;
use App\Services\GameRules\FirstWinRule;
use App\Services\GameRules\SecondWinRule;
use App\Services\GameService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class PlayRockPaperScissors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'play:rock-paper-scissors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Play rock-paper-scissors';

    public function __construct(
        private readonly GameService $gameService
    ) {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // Load rules
        $this->gameService->loadRule(App::make(DrawRule::class));
        $this->gameService->loadRule(App::make(FirstWinRule::class));
        $this->gameService->loadRule(App::make(SecondWinRule::class));

        $this->gameService->playGame();
        $gameStatistic = $this->gameService->getStatistics();
        foreach ($gameStatistic as $key => $value) {
            $this->renderStatistics($value, $key);
        }
    }

    private function renderStatistics(array $playerStatistics, string $playerName): void
    {
        $this->info("Player {$playerName} statistics:");
        $this->line("Wins: {$playerStatistics['wins']}, Losses: {$playerStatistics['losses']}, Draws: {$playerStatistics['draws']}");
    }
}
